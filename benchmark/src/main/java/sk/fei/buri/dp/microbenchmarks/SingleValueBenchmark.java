package sk.fei.buri.dp.microbenchmarks;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import sk.fei.buri.dp.ClassicJobService;
import sk.fei.buri.dp.ReactiveJobService;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@Warmup(iterations = 3, time = 3)
@Measurement(iterations = 3, time = 3)
@OutputTimeUnit(TimeUnit.SECONDS)
public class SingleValueBenchmark {

    private ClassicJobService classicJobService;
    private ReactiveJobService reactiveJobService;

    @Setup(Level.Iteration)
    public void setup() {
        classicJobService = new ClassicJobService();
        reactiveJobService = new ReactiveJobService();
    }

    /* ----- Microbenchmars for CLASSIC jobs ----- */

    @Benchmark
    public void classic_cpuBound_imperative(Blackhole blackhole) {
        blackhole.consume(
            classicJobService.cpuBoundJob_single_imperative()
        );
    }

    /* ----- Microbenchmars for REACTIVE jobs ----- */

    @Benchmark
    public void reactive_cpuBound_imperative(Blackhole blackhole) {
        CountDownLatch latch = new CountDownLatch(1);
        reactiveJobService.cpuBoundJob_single()
            .subscribe(blackhole::consume,
                       throwable -> {
                           throwable.printStackTrace();
                           latch.countDown();
                       },
                       latch::countDown
            );
    }

    public static void main(String[] args) throws RunnerException {
        Options options = new OptionsBuilder()
            .include(SingleValueBenchmark.class.getSimpleName())
            .forks(1)
            .build();
        new Runner(options).run();
    }
}
