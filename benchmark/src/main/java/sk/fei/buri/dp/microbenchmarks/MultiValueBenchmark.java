package sk.fei.buri.dp.microbenchmarks;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import sk.fei.buri.dp.ClassicJobService;
import sk.fei.buri.dp.ReactiveJobService;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.Throughput)
@Warmup(iterations = 3, time = 3)
@Measurement(iterations = 3, time = 3)
@OutputTimeUnit(TimeUnit.SECONDS)
public class MultiValueBenchmark {

    private ClassicJobService classicJobService;
    private ReactiveJobService reactiveJobService;

    @Param({"1000000"})
    private int request;

    @Setup(Level.Iteration)
    public void setup() {
        classicJobService = new ClassicJobService();
        reactiveJobService = new ReactiveJobService();
    }

    /* ----- Microbenchmarks for CLASSIC jobs ----- */

    @Benchmark
    public void classic_cpuBound_many_imperative(Blackhole blackhole) {
        classicJobService.cpuBoundJob_many_imperative(request, blackhole::consume);
    }

    @Benchmark
    public void classic_cpuBound_many_sequential_stream(Blackhole blackhole) {
        classicJobService.cpuBoundJob_many_sequential_stream(request)
            .forEach(blackhole::consume);
    }

    @Benchmark
    public void classic_cpuBound_many_parallel_stream(Blackhole blackhole) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch((request / 2) - 1);
        classicJobService.cpuBoundJob_many_parallel_stream(request)
            .forEach(result -> {
                latch.countDown();
                blackhole.consume(result);
            });
        latch.await();
    }

    @Benchmark
    public void classic_ioBound_fileProcessing_async(Blackhole blackhole) throws IOException {
        classicJobService.ioBoundJob_fileProcessing_async()
            .forEach(blackhole::consume);
    }

    @Benchmark
    public void classic_ioBound_fileProcessing_sequential(Blackhole blackhole) throws IOException {
        classicJobService.ioBound_fileProcessing_sequential()
            .forEach(blackhole::consume);
    }

    @Benchmark
    public void classic_ioBound_networkRequests_async(Blackhole blackhole) throws InterruptedException {
        classicJobService.ioBoundJob_networkRequests_async()
            .forEach(blackhole::consume);
    }

    @Benchmark
    public void classic_ioBound_networkRequests_sequential(Blackhole blackhole) throws IOException, InterruptedException {
        classicJobService.ioBoundJob_networkRequests_sequential()
            .forEach(blackhole::consume);
    }

    /* ----- Microbenchmarks for REACTIVE jobs ----- */

    @Benchmark
    public void reactive_cpuBound_many(Blackhole blackhole) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        reactiveJobService.cpuBoundJob_many(request)
            .subscribe(blackhole::consume,
                       throwable -> {
                           throwable.printStackTrace();
                           latch.countDown();
                       },
                       latch::countDown
            );
        latch.await();
    }

    @Benchmark
    public void reactive_cpuBound_many_parallel(Blackhole blackhole) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        reactiveJobService.cpuBoundJob_many_parallel(request)
            .subscribe(blackhole::consume,
                       throwable -> {
                           throwable.printStackTrace();
                           latch.countDown();
                       },
                       latch::countDown
            );
        latch.await();
    }

    @Benchmark
    public void reactive_ioBound_fileProcessing_sequential(Blackhole blackhole) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        reactiveJobService.ioBoundJob_fileProcessing_sequential()
            .subscribe(blackhole::consume,
                       throwable -> {
                           throwable.printStackTrace();
                           latch.countDown();
                       },
                       latch::countDown
            );
        latch.await();
    }

    @Benchmark
    public void reactive_ioBound_fileProcessing_async(Blackhole blackhole) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        reactiveJobService.ioBoundJob_fileProcessing_async()
            .subscribe(blackhole::consume,
                       throwable -> {
                           throwable.printStackTrace();
                           latch.countDown();
                       },
                       latch::countDown
            );
        latch.await();
    }

    @Benchmark
    public void reactive_ioBound_networkRequests(Blackhole blackhole) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        reactiveJobService.ioBoundJob_networkRequests()
            .subscribe(blackhole::consume,
                       throwable -> {
                           throwable.printStackTrace();
                           latch.countDown();
                       },
                       latch::countDown
            );
        latch.await();
    }

    public static void main(String[] args) throws RunnerException {
        Options options = new OptionsBuilder()
            .include(MultiValueBenchmark.class.getSimpleName())
            .forks(1)
            .build();
        new Runner(options).run();
    }
}
