import java.time.Duration;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import java.util.function.Supplier;
import java.util.stream.Stream;

import io.gatling.javaapi.core.CoreDsl;
import io.gatling.javaapi.core.ScenarioBuilder;
import io.gatling.javaapi.core.Simulation;
import io.gatling.javaapi.http.HttpDsl;
import io.gatling.javaapi.http.HttpProtocolBuilder;


import static io.gatling.javaapi.core.CoreDsl.*;
import static io.gatling.javaapi.http.HttpDsl.http;
import static io.gatling.javaapi.http.HttpDsl.status;

public class GatlingLoadTest extends Simulation {
    HttpProtocolBuilder httpProtocol = HttpDsl.http
        .baseUrl("http://localhost:8080");

    Iterator<Map<String, Object>> feeder =
        Stream.generate((Supplier<Map<String, Object>>) ()
            -> Collections.singletonMap("name", UUID.randomUUID().toString())
        ).iterator();

    // Scenario
    //    ScenarioBuilder scn = CoreDsl.scenario("Load Test Creating Customers")
    //        .exec(http("request")
    //                  .get("/endpoint")
    //                  .check(status().is(200))
    //        );

    ScenarioBuilder scn = CoreDsl.scenario("Load Test Customers")
        .feed(feeder)
        .exec(http("create-customer-req")
                  .post("/customer/#{name}")
                  .check(status().is(201))
                  .check(bodyString().saveAs("id"))
        )
        .exec(http("get-customer-req")
                  .get(session -> "/customer/" + session.getString("id"))
                  .check(bodyString().is(StringBody("#{name}")))
                  .check(status().is(200))
        )
        .exec(http("delete-customer-req")
                  .delete(session -> "/customer/" + session.getString(StringBody("#{name}").toString()))
                  .check(status().is(204))
        )
        .exec(http("get-all-customers-req")
                   .get(session -> "/customer")
                   .check(status().is(200))
        );

    public GatlingLoadTest() {
        this.setUp(scn.injectClosed(incrementConcurrentUsers(250)
                                        .times(8)
                                        .eachLevelLasting(20)
                                        .separatedByRampsLasting(10)
                                        .startingFrom(0)
        )).maxDuration(120).protocols(httpProtocol);
    }
}
