package sk.fei.buri.dp;

import java.io.BufferedWriter;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;

public class IOHelper {
    private static final Object monitor = new Object();

    public static final Path inFilePath = Path.of("files-resources/frankenstein_book.txt");
    public static final Path outFilePath = Path.of("files-resources/out.txt");

    public static final String REMOTE_API_URL = "http://localhost:8090/hello";
    public static final HttpClient httpClient = HttpClient.newHttpClient();
    public static final HttpRequest httpRequest = HttpRequest
        .newBuilder(
            URI.create(REMOTE_API_URL))
        .GET()
        .build();

    public static void closeOutFile(BufferedWriter writer) throws IOException {
        writer.close();
    }

    /**
     * Open out file through {@link BufferedWriter}, empty it and return created writer.
     *
     * @return {@link BufferedWriter} of out file.
     * @throws IOException if some file operation failed
     */
    public static BufferedWriter openAndEmptyOutFile() throws IOException {
        BufferedWriter writer = Files.newBufferedWriter(outFilePath);

        writer.write("");
        writer.flush();

        return writer;
    }

    /**
     * Write number to file in separate line through {@link BufferedWriter},
     *
     * @param writer - file in which write will be performed
     * @param number - number to be written
     * @throws IOException - if write to file failed
     */
    public static void writeToOutFile(BufferedWriter writer, int number) throws IOException {
        writer.write(number + "\n");
        writer.flush();
    }

    /**
     * Write number to file asynchronously in separate line through {@link BufferedWriter},
     *
     * @param writer - file in which write will be performed
     * @param number - number to be written
     * @return {@link CompletableFuture} of {@link Integer} which represents the number to be written in file.
     */
    public static CompletableFuture<Integer> makeAsyncWrite(BufferedWriter writer, int number) {
        return CompletableFuture.supplyAsync(() -> {
            synchronized (monitor) {
                try {
                    writeToOutFile(writer, number);
                    return number;
                } catch (IOException err) {
                    err.printStackTrace();
                    return -1;
                }
            }
        });
    }

    /**
     * Perform the pre-defined HTTP request on remote HTTP server.
     * Precondition: Remote HTTP server in remote-http-server.go file should be run!
     *
     * @return response in {@link HttpResponse} of {@link String}
     * @throws IOException
     * @throws InterruptedException
     */
    public static HttpResponse<String> performHttpRequest() throws IOException, InterruptedException {
        return httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
    }

    /**
     * Perform the pre-defined HTTP request asynchronously on remote HTTP server.
     * Precondition: Remote HTTP server in remote-http-server.go file should be run!
     *
     * @return response in {@link HttpResponse} of {@link String}
     * @throws IOException
     * @throws InterruptedException
     */
    public static CompletableFuture<HttpResponse<String>> performHttpRequestAsync() {
        return httpClient.sendAsync(httpRequest, HttpResponse.BodyHandlers.ofString());
    }
}
