package sk.fei.buri.dp.db;

import org.springframework.data.r2dbc.repository.Modifying;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface CustomerRepository extends R2dbcRepository<Customer, Integer> {

    @Query("DELETE FROM customer WHERE name = :name")
    @Modifying
    Mono<Void> deleteCustomerByName(@Param("name") String name);
}