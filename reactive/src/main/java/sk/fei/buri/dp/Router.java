package sk.fei.buri.dp;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;


import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
public class Router {

    private final Handler handler;

    public Router(Handler handler) {
        this.handler = handler;
    }

    @Bean
        // functional router - change HandlerFunction in parameter to test your desired method
    RouterFunction<ServerResponse> endpoint() {
        return route(GET("/endpoint"),
                     req -> handler.createInternalDelay().flatMap(result -> ServerResponse.ok().bodyValue(result))
        );
    }

    @Bean
    RouterFunction<ServerResponse> customerEndpoints() {
        return route(POST("/customer/{name}"),
                     req -> handler.createCustomer(req.pathVariable("name"))
                         .flatMap(customer -> ServerResponse.status(201).bodyValue(customer.getId()))
        ).and(route(GET("/customer/{id}"),
                    req -> handler.getCustomer(Integer.valueOf(req.pathVariable("id")))
                        .flatMap(customer -> ServerResponse.ok().bodyValue(customer.getName())
                            .switchIfEmpty(ServerResponse.notFound().build()))
        )).and(route(DELETE("/customer/{name}"),
                     req -> handler.deleteCustomer(req.pathVariable("name"))
                         .then(ServerResponse.status(204).build())
        )).and(route(GET("/customer"),
                     req -> handler.getAllCustomers().collectList()
                         .flatMap(customers -> ServerResponse.ok().bodyValue(customers))
        ));
    }
}
