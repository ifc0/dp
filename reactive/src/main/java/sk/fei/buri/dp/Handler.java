package sk.fei.buri.dp;

import java.nio.file.Path;
import java.time.Duration;
import java.util.UUID;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import sk.fei.buri.dp.db.Customer;
import sk.fei.buri.dp.db.CustomerRepository;


import static sk.fei.buri.dp.IOHelper.REMOTE_API_URL;


@Component
public class Handler {

    private final WebClient webClient;
    private final CustomerRepository customerRepository;

    private static final Duration ONE_SECOND_DURATION = Duration.ofSeconds(1);

    public Handler(WebClient webClient, CustomerRepository customerRepository) {
        this.webClient = webClient;
        this.customerRepository = customerRepository;
    }

    /**
     * A method to send an HTTP request to remote API.
     *
     * @return response of {@link Mono}<{@link String}> from remote API call.
     */
    public Mono<String> sendNetworkRequest() {
        return webClient
            .get()
            .uri(REMOTE_API_URL)
            .retrieve()
            .bodyToMono(String.class);
    }

    /**
     * Create an one-second internal delay and returns a hardcoded "Hello" response.
     *
     * @return {@link Mono}<{@link String}> value of "Hello"
     */
    public Mono<String> createInternalDelay() {
        return Mono.just("Hello")
            .delayElement(ONE_SECOND_DURATION);
    }

    /**
     * Create new customer.
     *
     * @return new created {@link Customer}
     */
    public Mono<Customer> createCustomer(String name) {
        return customerRepository.save(new Customer(null, name));
    }

    public Mono<Customer> getCustomer(Integer id) {
        return customerRepository.findById(id);
    }

    public Mono<Void> deleteCustomer(String name) {
        return customerRepository.deleteCustomerByName(name);
    }

    public Flux<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    private Path generateRandomFileName() {
        return Path.of("generated-files/" + UUID.randomUUID());
    }
}
