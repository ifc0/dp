package sk.fei.buri.dp;

import java.io.BufferedWriter;
import java.io.IOException;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.BaseStream;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.ParallelFlux;
import reactor.core.scheduler.Schedulers;


import static sk.fei.buri.dp.IOHelper.*;

public class ReactiveJobService {

    private BufferedWriter out;

    public Mono<String> cpuBoundJob_single() {
        return Mono.fromSupplier(() -> ThreadLocalRandom.current().nextInt())
            .map(Math::decrementExact)
            .map(Integer::toHexString)
            .flatMap(hexString -> Mono.just("( " + hexString + " )"));
    }

    public Flux<String> cpuBoundJob_many(int request) {
        return Flux.range(0, request)
            .map(Math::decrementExact)
            .filter(n -> n % 2 == 1)
            .map(Double::toHexString)
            .concatMap(hexString -> Mono.just("( " + hexString + " )"));
    }

    public ParallelFlux<String> cpuBoundJob_many_parallel(int request) {
        return Flux.range(0, request)
            .parallel()
            .runOn(Schedulers.parallel())
            .map(Math::decrementExact)
            .filter(n -> n % 2 == 1)
            .map(Double::toHexString)
            .flatMap(hexString -> Mono.just("( " + hexString + " )"));
    }

    public Flux<Integer> ioBoundJob_fileProcessing_sequential() {
        // Flux.using because of proper releasing system resources
        return Flux.using(() -> Files.lines(inFilePath),
                          Flux::fromStream,
                          BaseStream::close
            ).filter(line -> !line.isEmpty())
            .map(String::length)
            .map(cnt -> {
                try {
                    writeToOutFile(out, cnt);
                    return cnt;
                } catch (IOException err) {
                    err.printStackTrace();
                    return -1;
                }
            })
            // file operations are executed on thread pool dedicated for blocking calls
            .subscribeOn(Schedulers.boundedElastic())
            .doOnSubscribe($ /* '$' symbol is used when input is not needed */ -> {
                try {
                    out = openAndEmptyOutFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            })
            .doFinally($ -> {
                try {
                    closeOutFile(out);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
    }

    public Flux<Integer> ioBoundJob_fileProcessing_async() {
        // Flux.using because of proper releasing system resources
        return Flux.using(() -> Files.lines(inFilePath),
                          Flux::fromStream,
                          BaseStream::close
            ).filter(line -> !line.isEmpty())
            .map(String::length)
            .flatMap(cnt -> Mono.fromFuture(makeAsyncWrite(out, cnt)))
            .doOnSubscribe($ /* '$' symbol is used when input is not needed */ -> {
                try {
                    out = openAndEmptyOutFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            })
            .doFinally($ -> {
                try {
                    closeOutFile(out);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
    }

    public Flux<Integer> ioBoundJob_networkRequests() {
        return Flux.merge(
                Mono.fromFuture(httpClient.sendAsync(httpRequest, HttpResponse.BodyHandlers.ofString())),
                Mono.fromFuture(httpClient.sendAsync(httpRequest, HttpResponse.BodyHandlers.ofString())),
                Mono.fromFuture(httpClient.sendAsync(httpRequest, HttpResponse.BodyHandlers.ofString())),
                Mono.fromFuture(httpClient.sendAsync(httpRequest, HttpResponse.BodyHandlers.ofString())),
                Mono.fromFuture(httpClient.sendAsync(httpRequest, HttpResponse.BodyHandlers.ofString()))
            )
            .map(HttpResponse::body)
            .map(String::length);
    }
}
