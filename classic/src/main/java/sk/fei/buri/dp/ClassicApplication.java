package sk.fei.buri.dp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableJdbcRepositories
public class ClassicApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClassicApplication.class, args);
    }

    // global RestTemplate bean used across whole module
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
