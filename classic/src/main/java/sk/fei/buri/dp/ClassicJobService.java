package sk.fei.buri.dp;

import java.io.BufferedWriter;
import java.io.IOException;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;


import static sk.fei.buri.dp.IOHelper.*;

public class ClassicJobService {

    private BufferedWriter out;

    public String cpuBoundJob_single_imperative() {
        return "( " + Integer.toHexString(Math.abs(new Random().nextInt())) + " )";
    }

    public void cpuBoundJob_many_imperative(int request, Consumer<String> consumer) {
        for (int i = 0; i < request; i++) {
            int n = Math.decrementExact(i);
            if (n % 2 == 1) {
                consumer.accept("( " + Integer.toHexString(n) + " )");
            }
        }
    }

    public Stream<String> cpuBoundJob_many_sequential_stream(int request) {
        return IntStream.range(0, request)
            .boxed()
            .map(Math::decrementExact)
            .filter(n -> n % 2 == 1)
            .map(Integer::toHexString)
            .map(hexString -> "( " + hexString + " )");
    }

    public Stream<String> cpuBoundJob_many_parallel_stream(int request) {
        return IntStream.range(0, request)
            .boxed()
            .parallel()
            .map(Math::decrementExact)
            .filter(n -> n % 2 == 1)
            .map(Integer::toHexString)
            .map(hexString -> "( " + hexString + " )");
    }

    public List<Integer> ioBoundJob_fileProcessing_async() throws IOException {
        final List<Integer> numbers = new ArrayList<>();
        AtomicInteger activeJobsRemaining = new AtomicInteger(0);
        AtomicBoolean started = new AtomicBoolean(false);
        try (Stream<String> stream = Files.lines(inFilePath)) {
            out = openAndEmptyOutFile();
            stream
                .filter(line -> !line.isEmpty())
                .map(String::length)
                .map(number -> makeAsyncWrite(out, number))
                .forEach(future -> {
                    activeJobsRemaining.incrementAndGet();
                    started.compareAndSet(false, true);
                    future.thenAccept(number -> {
                        numbers.add(number);
                        activeJobsRemaining.decrementAndGet();
                    });
                });
        }
        while (!started.get() || (started.get() && activeJobsRemaining.get() != 0)) {
            // barrier for main thread
        }
        closeOutFile(out);
        out = null;
        return numbers;
    }

    public List<Integer> ioBound_fileProcessing_sequential() throws IOException {
        final List<Integer> numbers;
        try (Stream<String> stream = Files.lines(inFilePath)) {
            out = openAndEmptyOutFile();
            numbers = stream
                .filter(line -> !line.isEmpty())
                .map(String::length)
                .map(number -> {
                    try {
                        writeToOutFile(out, number);
                        return number;
                    } catch (IOException e) {
                        e.printStackTrace();
                        return -1;
                    }
                })
                .collect(Collectors.toList());
        }
        closeOutFile(out);
        out = null;
        return numbers;
    }

    public List<HttpResponse<String>> ioBoundJob_networkRequests_sequential() throws IOException,
        InterruptedException {
        List<HttpResponse<String>> responses = new ArrayList<>(5);
        for (int i = 0; i < 5; i++) {
            responses.add(performHttpRequest());
        }
        return responses;
    }

    public List<HttpResponse<String>> ioBoundJob_networkRequests_async() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(5);
        List<HttpResponse<String>> responses = new ArrayList<>(5);
        for (int i = 0; i < 5; i++) {
            performHttpRequestAsync()
                .thenAccept(e -> {
                    responses.add(e);
                    latch.countDown();
                });
        }
        latch.await();
        return responses;
    }
}
