package sk.fei.buri.dp;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import sk.fei.buri.dp.db.Customer;
import sk.fei.buri.dp.db.CustomerRepository;


import static sk.fei.buri.dp.IOHelper.REMOTE_API_URL;

@RestController
public class Controller {

    private final RestTemplate restTemplate;
    private final CustomerRepository customerRepository;

    public Controller(RestTemplate restTemplate, CustomerRepository customerRepository) {
        this.restTemplate = restTemplate;
        this.customerRepository = customerRepository;

    }

    @GetMapping("/endpoint")
    // imperative router - change method's body to test your desired method
    public String getEndpoint() throws IOException {
        return writeToFile();
    }

    @PostMapping("/customer/{name}")
    public ResponseEntity<Integer> createCustomer(@PathVariable String name) {
        return ResponseEntity.status(201).body(customerRepository.save(new Customer(null, name)).getId());
    }

    @GetMapping("/customer/{id}")
    public ResponseEntity<String> getCustomer(@PathVariable Integer id) {
        return ResponseEntity.status(200).body(customerRepository.findById(id).get().getName());
    }

    @DeleteMapping("/customer/{name}")
    public ResponseEntity<Void> deleteCustomer(@PathVariable String name) {
        customerRepository.deleteCustomer(name);
        return ResponseEntity.status(204).build();
    }

    @GetMapping("/customer")
    public ResponseEntity<Iterable<Customer>> getAllCustomers() {
        return ResponseEntity.ok(customerRepository.findAll());
    }

    /**
     * A method to send an HTTP request to remote API.
     *
     * @return response of {@link String} from remote API call.
     */
    public String sendNetworkRequest() {
        return restTemplate.getForObject(REMOTE_API_URL, String.class);
    }

    /**
     * Create an one-second internal delay and returns a hardcoded "Hello" response.
     *
     * @return {@link String} value of "Hello"
     */
    public String createInternalDelay() throws InterruptedException {
        Thread.sleep(1000);
        return "Hello";
    }

    /**
     * Write empty string to file with random UUID file name.
     *
     * @return {@link String} value "Done!" representing the operation completion.
     */
    public String writeToFile() throws IOException {
        Files.write(generateRandomFileName(), "".getBytes(StandardCharsets.UTF_8));
        return "Done!";
    }

    private static Path generateRandomFileName() {
        return Path.of("generated-files/" + UUID.randomUUID());
    }
}
