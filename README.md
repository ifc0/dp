# DP

Diploma thesis written on Slovak Technical University for testing imperative and reactive application using microbenchmarks and Gatling load tests.

## Description

Multi-module *Maven* project written in *Java* consisting of the following modules:

* **benchmark** - JMH microbenchmarks and Gatling load tests for testing imperative and reactive code
* **reactive** - code and Spring Boot app for reactive model
* **classic** - code and Spring Boot app for imperative model
* **helper** - helper methods for I/O operations

## Usage

If you want to run *JMH* microbenchmark, you have to choose one of the provided JMH runners in *benchmark* module and run it directly in IDE (running
JAR is not supported).

If you want to run *Gatling* load tests, you have to adjust code for methods you want to test, run one of *Spring Boot* apps separated in modules and
execute command:

```bash
cd benchmark/ && mvn gatling:test
```

To test HTTP requests to a remote server, you have to start the remote HTTP server according to the information in the *IOHelper* class or start a
pre-prepared Go HTTP server via the command:

```bash
$ go run remote-http-server.go
```

In case you want to run load tests with database operations, you need to install *PostgreSQL* locally. Database configuration can be found in
project's source code. If you are using *Podman* tool for managing containers, you can easily prepare database environment with the command:

```bash
podman run -d --name postgres -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=postgres -v postgres:/var/lib/postgresql/data postgres:14 -c max_connections=110
```

Each of tests provides a report in console output.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)

## Contact

Author: Ivan Búri

Email: ivan.buri@gmail.com